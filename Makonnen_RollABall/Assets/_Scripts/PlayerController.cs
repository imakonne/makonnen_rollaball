﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour {
    //floats for speed of ball as well as the count text and win text
    public float speed;
    public Text countText;
    public Text winText;

    Rigidbody rb;
    private int count;
    Renderer Therend;



    void Start ()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
        Therend = GetComponent<Renderer>();
        //our functions/ variables
    }

    void FixedUpdate ()
{
        //the movement of the ball/player, how it moves, whether horizontally, vertically, etc
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3 (-moveHorizontal, 0.0f, -moveVertical);

        rb.AddForce(movement * speed);
    }
    void OnTriggerEnter(Collider other)
        {
        //this bit of code helps the counter after the objects are picked up, hence the count = count+1 code
            if(other.gameObject.CompareTag ( "pick up"))
            {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText ();

            //ONE OF MY SCRIPT MODIFICATIONS 1/2
            //this piece of code is for the ball, after it picks up an object.. This script makes the ball bigger after every time it gets a pick up.
            transform.localScale += new Vector3(.1f, .1f, .1f);
            speed += 1f;
            }
            //SCRIPT MODIFICATION 2/2
            //connor helped..
        //what this line of code does is change the player object or the ball to whatever the pick up color is... so when the ball picks up an object, it takes that material color of that pick up
        Therend.material = other.gameObject.GetComponent<Renderer>().material;        }


    //CountText, pretty self explanitory, Counts the pickups after they get picked up, and also displays a you win text, after the player has picked up all of the objects.
    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 12)
        {
            winText.text = "YOU WIN!";
        }
    }// what the text displays after the user picks up all 5 pick up objects. "you win!""

    }

